package wiki.xsx.core.log;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import wiki.xsx.core.config.Slf4jProperties;
import wiki.xsx.core.support.LogContext;
import wiki.xsx.core.support.LogHandler;
import wiki.xsx.core.support.MethodInfo;
import wiki.xsx.core.support.MethodParser;

import javax.annotation.Resource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 日志处理器
 * @author xsx
 * @date 2019/6/17
 * @since 1.8
 */
@Aspect
@Slf4j
public class LogProcessor {

    @Resource(type = Slf4jProperties.class)
    private Slf4jProperties slf4jProperties;

    /**
     * 打印参数日志
     * @param joinPoint 切入点
     */
    @Before("@annotation(ParamLog)")
    public void beforePrint(JoinPoint joinPoint) {
        if (LogHandler.isEnable(log)) {
            MethodSignature signature  = (MethodSignature) joinPoint.getSignature();
            ParamLog annotation = signature.getMethod().getAnnotation(ParamLog.class);
            Level level = annotation.level()==Level.UNKNOWN?this.slf4jProperties.getGlobalParamLogLevel():annotation.level();
            Position position = annotation.position()==Position.UNKNOWN?this.slf4jProperties.getGlobalParamLogPosition():annotation.position();
            MethodInfo methodInfo = this.beforePrint(
                    signature,
                    joinPoint.getArgs(),
                    annotation.paramFilter(),
                    annotation.value(),
                    level,
                    position,
                    annotation.formatter()
            );
            // 执行回调
            this.callback(annotation.callback(), annotation, methodInfo, joinPoint, null);
        }
    }

    /**
     * 打印返回值日志
     * @param joinPoint 切入点
     * @param result 返回结果
     */
    @AfterReturning(value = "@annotation(ResultLog)", returning = "result")
    public void afterPrint(JoinPoint joinPoint, Object result) {
        if (LogHandler.isEnable(log)) {
            MethodSignature signature  = (MethodSignature) joinPoint.getSignature();
            ResultLog annotation = signature.getMethod().getAnnotation(ResultLog.class);
            Level level = annotation.level()==Level.UNKNOWN?this.slf4jProperties.getGlobalResultLogLevel():annotation.level();
            Position position = annotation.position()==Position.UNKNOWN?this.slf4jProperties.getGlobalResultLogPosition():annotation.position();
            MethodInfo methodInfo = this.afterPrint(
                    signature,
                    result,
                    annotation.value(),
                    level,
                    position,
                    annotation.formatter()
            );
            // 执行回调
            this.callback(annotation.callback(), annotation, methodInfo, joinPoint, result);
        }
    }

    /**
     * 打印异常日志
     * @param joinPoint 切入点
     * @param throwable 异常
     */
    @AfterThrowing(value = "@annotation(ThrowingLog)||@annotation(Log)", throwing = "throwable")
    public void throwingPrint(JoinPoint joinPoint, Throwable throwable) {
        if (LogHandler.isEnable(log)) {
            MethodSignature signature  = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            String methodName = method.getName();
            try {
                Annotation annotation;
                String busName;
                Class<? extends LogCallback> callback;
                MethodInfo methodInfo = MethodParser.getMethodInfo(MethodInfo.NATIVE_LINE_NUMBER, signature);
                ThrowingLog throwingLogAnnotation = method.getAnnotation(ThrowingLog.class);
                if (throwingLogAnnotation!=null) {
                    annotation = throwingLogAnnotation;
                    busName = throwingLogAnnotation.value();
                    callback = throwingLogAnnotation.callback();
                }else {
                    Log logAnnotation = method.getAnnotation(Log.class);
                    annotation = logAnnotation;
                    busName = logAnnotation.value();
                    callback = logAnnotation.callback();
                }
                log.error(LogHandler.getThrowingInfo(busName, methodInfo)+throwable.getLocalizedMessage(), throwable);
                // 执行回调
                this.callback(callback, annotation, methodInfo, joinPoint, null);
            } catch (Exception e) {
                log.error("{}.{}方法错误", signature.getDeclaringTypeName(), methodName);
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 打印环绕日志
     * @param joinPoint 切入点
     * @return 返回方法返回值
     * @throws Throwable 异常
     */
    @Around(value = "@annotation(Log)")
    public Object aroundPrint(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature  = (MethodSignature) joinPoint.getSignature();
        Object[] args = joinPoint.getArgs();
        Object result;
        if (LogHandler.isEnable(log)) {
            long beginTime = System.currentTimeMillis();
            long endTime;
            try {
                result = joinPoint.proceed(args);
                endTime = System.currentTimeMillis();
            }catch (Exception e) {
                result = e;
                endTime = System.currentTimeMillis();
            }
            MethodInfo methodInfo;
            try {
                Log annotation = signature.getMethod().getAnnotation(Log.class);
                Level level = annotation.level()==Level.UNKNOWN?this.slf4jProperties.getGlobalLogLevel():annotation.level();
                Position position = annotation.position()==Position.UNKNOWN?this.slf4jProperties.getGlobalLogPosition():annotation.position();
                if (position==Position.ENABLED||(position==Position.UNKNOWN&&level==Level.DEBUG)) {
                    methodInfo = MethodParser.getMethodInfo(signature);
                }else {
                    methodInfo = MethodParser.getMethodInfo(MethodInfo.NATIVE_LINE_NUMBER, signature);
                }
                methodInfo.setTakeTime(endTime-beginTime);
                LogFormatter formatter;
                Class<? extends LogFormatter> formatType = annotation.formatter();
                if (!formatType.getName().equals(DefaultLogFormatter.DEFAULT)) {
                    formatter = LogContext.getContext().getBean(formatType);
                }else {
                    formatter = LogContext.getContext().getBean(this.slf4jProperties.getGlobalLogFormatter());
                }
                formatter.format(log, level, annotation.value(), methodInfo, args, annotation.paramFilter(), result);
                // 执行回调
                this.callback(annotation.callback(), annotation, methodInfo, joinPoint, result);
            }catch (Exception e) {
                log.error("{}.{}方法错误", signature.getDeclaringTypeName(), signature.getMethod().getName());
                log.error(e.getMessage(), e);
            }
            if (result instanceof Throwable) {
                throw (Throwable) result;
            }
        }else {
            result = joinPoint.proceed(args);
        }
        return result;
    }

    /**
     * 打印参数日志
     * @param signature 方法签名
     * @param args 参数列表
     * @param filterParamNames 参数过滤列表
     * @param busName 业务名称
     * @param level 日志级别
     * @param position 代码定位开启标志
     * @return 返回方法信息
     */
    private MethodInfo beforePrint(
            MethodSignature signature,
            Object[] args,
            String[] filterParamNames,
            String busName,
            Level level,
            Position position,
            Class<? extends ParamLogFormatter> paramFormatter
    ) {
        MethodInfo methodInfo = null;
        try {
            if (position==Position.ENABLED||(position==Position.UNKNOWN&&level==Level.DEBUG)) {
                methodInfo = MethodParser.getMethodInfo(signature);
            }else {
                methodInfo = MethodParser.getMethodInfo(MethodInfo.NATIVE_LINE_NUMBER, signature);
            }
            ParamLogFormatter formatter;
            if (!paramFormatter.getName().equals(DefaultParamLogFormatter.DEFAULT)) {
                formatter = LogContext.getContext().getBean(paramFormatter);
            }else {
                formatter = LogContext.getContext().getBean(this.slf4jProperties.getGlobalParamLogFormatter());
            }
            formatter.format(log, level, busName, methodInfo, args, filterParamNames);
        } catch (Exception e) {
            log.error("{}.{}方法错误", signature.getDeclaringTypeName(), signature.getMethod().getName());
            log.error(e.getMessage(), e);
        }
        return methodInfo;
    }

    /**
     * 打印返回值日志
     * @param signature 方法签名
     * @param result 返回结果
     * @param busName 业务名称
     * @param level 日志级别
     * @param position 代码定位开启标志
     * @return 返回方法信息
     */
    private MethodInfo afterPrint(
            MethodSignature signature,
            Object result,
            String busName,
            Level level,
            Position position,
            Class<? extends ResultLogFormatter> resultFormatter
    ) {
        MethodInfo methodInfo = null;
        try {
            if (position==Position.ENABLED||(position==Position.UNKNOWN&&level==Level.DEBUG)) {
                methodInfo = MethodParser.getMethodInfo(signature);
            }else {
                methodInfo = MethodParser.getMethodInfo(MethodInfo.NATIVE_LINE_NUMBER, signature);
            }
            ResultLogFormatter formatter;
            if (!resultFormatter.getName().equals(DefaultResultLogFormatter.DEFAULT)) {
                formatter = LogContext.getContext().getBean(resultFormatter);
            }else {
                formatter = LogContext.getContext().getBean(this.slf4jProperties.getGlobalResultLogFormatter());
            }
            formatter.format(log, level, busName, methodInfo, result);
        } catch (Exception e) {
            log.error("{}.{}方法错误", signature.getDeclaringTypeName(), signature.getMethod().getName());
            log.error(e.getMessage(), e);
        }
        return methodInfo;
    }

    /**
     * 执行回调
     * @param callback 回调类
     * @param annotation 触发注解
     * @param methodInfo 方法信息
     * @param joinPoint 切入点
     * @param result 方法结果
     */
    private void callback(
            Class<? extends LogCallback> callback,
            Annotation annotation,
            MethodInfo methodInfo,
            JoinPoint joinPoint,
            Object result
    ) {
        try {
            if (!callback.getName().equals(LogCallback.class.getName())) {
                LogContext.getContext().getBean(callback).callback(
                        annotation,
                        methodInfo,
                        LogHandler.getParamMap(methodInfo.getParamNames(), joinPoint.getArgs()),
                        result
                );
            }else {
                LogContext.getContext().getBean(this.slf4jProperties.getCallback(annotation)).callback(
                        annotation,
                        methodInfo,
                        LogHandler.getParamMap(methodInfo.getParamNames(), joinPoint.getArgs()),
                        result
                );
            }
        }catch (Exception ex) {
            log.error("{}.{}方法日志回调错误：【{}】", methodInfo.getClassAllName(), methodInfo.getMethodName(), ex.getMessage());
        }
    }
}
